/* 
 * File:   ControlPanel.h
 * Author: Revers
 *
 * Created on 26 kwiecień 2012, 20:33
 */

#ifndef CONTROLPANEL_H
#define	CONTROLPANEL_H

#include <boost/filesystem.hpp>
#include "Effects/AbstractEffect.h"
#include "Filters/AbstractFilter.h"

#include <GL/glfw.h>
#include <AntTweakBar.h>

#include <vector>
#include <map>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include <rev/gl/RevVBOQuadVT.h>
#include <rev/gl/RevGLTextureRenderTarget.h>
#include <rev/gl/RevGLTexture2D.h>

typedef std::map<std::string, AbstractEffect*> EffectMap;
typedef std::map<std::string, AbstractFilter*> FilterMap;

class ControlPanel {
    static log4cplus::Logger logger;
    typedef std::vector<boost::filesystem::path> PathVector;

    friend class AbstractEffect;
    friend class AbstractFilter;

    struct TweakBarBounds {
        int x;
        int y;
        int width;
        int height;
    };

    TweakBarBounds settingsBarBounds;
    TwBar *effectBar;
    TwBar *filterBar;
    TwBar *controlBar;

    AbstractEffect* currentEffect;
    AbstractFilter* currentFilter;

    char* currentEffectStr;
    char* currentFilterStr;

    PathVector backgroudImagesVector;
    int selectedBackgroundIndex;

    bool lmbPressed;

    int lastMouseWheelPos;
    float totalTime;
    float speed;
    bool aaOn;
    int aaLevel;
    float aaScale;
    rev::GLTexture2D texture;
    rev::GLTextureRenderTarget effectRenderTarget;
public:
    bool paused;
    
    FilterMap filterMap;
    EffectMap effectMap;

    
    rev::VBOQuadVT quadVBO;
    rev::VBOQuadVT secondQuadVBO;

    
    rev::GLTextureRenderTarget texRenderTarget;
    rev::GLTextureRenderTarget secondTexRenderTarget;
    int imageWidth;
    int imageHeight;
    int windowWidth;
    int windowHeight;
    int widthOffset;
    bool mouseOver;

    ControlPanel();

    virtual ~ControlPanel();

    void render();

    bool init();

    void reset() {
        totalTime = 0.0f;
    }

    void updateCallback(float msTime);

    const char* getCurrentEffectName() {
        if (!currentEffect) {
            return NULL;
        }
        return currentEffect->getName();
    }

    void setEffect(AbstractEffect* effect);

    AbstractEffect* getEffect() {
        return currentEffect;
    }
    
    const char* getCurrentFilterName() {
        if (!currentFilter) {
            return NULL;
        }
        return currentFilter->getName();
    }

    void setFilter(AbstractFilter* filter);

    AbstractFilter* getFilter() {
        return currentFilter;
    }

    void keyDownCallback(int key, int action) {
        TwEventKeyGLFW(key, action);
    }

    void resizeCallback(int width, int height);

    void mousePosCallback(int x, int y);

    void mouseButtonCallback(int id, int state);

    void mouseWheelCallback(int pos) {
        int diff = pos - lastMouseWheelPos;
        lastMouseWheelPos = pos;

        if (!mouseOver) {
            currentEffect->mouseWheelCallback(diff);
            currentFilter->mouseWheelCallback(diff);
        }

        refresh();
        TwEventMouseWheelGLFW(pos);
    }

    void refresh() {
        TwRefreshBar(effectBar);
        TwRefreshBar(controlBar);
    }

    int getAALevel() const {
        return aaLevel;
    }

    float getAAScale() const {
        return aaScale;
    }

    void setAALevel(int aaLevel);
    void setAAScale(float aaScale);
    
    void bindTexture();

private:
    bool changeTexture(const char* textureFile);

    static void TW_CALL setSpeedCallback(const void* value, void* clientData);
    static void TW_CALL getSpeedCallback(void* value, void* clientData);

    static void TW_CALL setParamACallback(const void* value, void* clientData);
    static void TW_CALL getParamACallback(void* value, void* clientData);

    static void TW_CALL setAALevelCallback(const void* value, void* clientData);
    static void TW_CALL getAALevelCallback(void* value, void* clientData);

    static void TW_CALL setAAScaleCallback(const void* value, void* clientData);
    static void TW_CALL getAAScaleCallback(void* value, void* clientData);

    static void TW_CALL copyCDStringToClient(char** destPtr, const char* src);

    static void TW_CALL resetTimeCallback(void* clientData);

    static void TW_CALL setBackgroundCallback(const void* value, void* clientData);
    static void TW_CALL getBackgroundCallback(void* value, void* clientData);

    static void TW_CALL resetEffectCallback(void* clientData);
    static void TW_CALL resetFilterCallback(void* clientData);


    bool loadBackgroundImages();

    bool getImagePathList(const char* directory,
            std::vector<boost::filesystem::path>& result);

    bool addEffects();
    bool addFilters();
};

#endif	/* CONTROLPANEL_H */

