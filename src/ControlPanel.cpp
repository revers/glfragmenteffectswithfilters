/* 
 * File:   ControlPanel.cpp
 * Author: Revers
 * 
 * Created on 26 kwiecień 2012, 20:33
 */
#include <GL/glew.h>

#include <iostream>
#include <boost/algorithm/string.hpp>

#include "ControlPanel.h"
#include "LoggingDefines.h"

#include "AllFilters.h"
#include "AllEffects.h"

#include "RenderTargetDefines.h"
#include "ToolbarCoordinates.h"

#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevStringUtil.h>

#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevAssert.h>

using namespace rev;
using namespace log4cplus;
using namespace boost::filesystem;
using namespace std;

#define X_MIN -0.4f
#define X_MAX 1.0f
#define Y_MIN -1.0f
#define Y_MAX 1.0f

Logger ControlPanel::logger = Logger::getInstance("ControlPanel");

inline std::string pathToString(const boost::filesystem::path& p) {
    const wchar_t* wFilename = p.c_str();
    return StringUtil::fromWideString(wFilename);
}

ControlPanel::ControlPanel() {

    texture.setInitialInternalFormat(GL_RGB8);
    //texture.setChannelsGL(
    texture.setInitialSampler(GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT);

    lmbPressed = false;
    currentEffect = NULL;
    currentEffectStr = NULL;
    currentFilter = NULL;
    currentFilterStr = NULL;
    selectedBackgroundIndex = 0;
    controlBar = NULL;
    effectBar = NULL;
    filterBar = NULL;
    mouseOver = false;
    widthOffset = 0;
    lastMouseWheelPos = 0;
    totalTime = 0.0f;
    speed = 1.0f;
    paused = true;

    aaOn = true;
    aaLevel = 1;
    aaScale = 0.006f;
}

ControlPanel::~ControlPanel() {
    for (EffectMap::iterator it = effectMap.begin();
            it != effectMap.end(); ++it) {
        delete it->second;
    }

    for (FilterMap::iterator it = filterMap.begin();
            it != filterMap.end(); ++it) {
        delete it->second;
    }

    TwTerminate();
}

bool ControlPanel::init() {
    if (!TwInit(TW_OPENGL_CORE, NULL)) {
        LOG_ERROR(logger, TwGetLastError());
        return false;
    }

    if (!quadVBO.create(X_MIN, X_MAX)) {
        LOG_ERROR(logger, "quadVBO.create(X_MIN, X_MAX)) FAILED!!");
        return false;
    }

    if (!secondQuadVBO.create()) {
        LOG_ERROR(logger, "secondQuadVBO.create() FAILED!!");
        return false;
    }

    TwDefine(" TW_HELP visible=false ");

    controlBar = TwNewBar("Control");
    TwDefine(" Control color='20 20 20' ");
    TwDefine(" Control valueswidth=165 fontSize=3 ");

    filterBar = TwNewBar("Filters");
    TwDefine(" Filters color='20 20 20' ");
    TwDefine(" Filters valueswidth=165 fontSize=3 ");

    effectBar = TwNewBar("Effects");
    TwDefine(" Effects color='20 20 20' ");
    TwDefine(" Effects valueswidth=165 fontSize=3 ");

    //TwAddSeparator(controlBar, "__contSep1", NULL);
    if (!loadBackgroundImages()) {
        LOG_ERROR(logger, "ControlPanel::loadBackgroundImages() FAILED!!");
        return false;
    }

    //TwAddVarRO(controlBar, "__AA", TW_TYPE_BOOLCPP, &(aaOn), "label='AA on/off' ");

    TwAddVarCB(controlBar, "__AALevel", TW_TYPE_INT32, setAALevelCallback, getAALevelCallback, this,
            "label='AA Level' min=1 max=10 step=1 ");

    TwAddVarCB(controlBar, "__AAScale", TW_TYPE_FLOAT, setAAScaleCallback, getAAScaleCallback, this,
            "label='AA Scale' min=0.001 max=25.0 step=0.001 ");


    TwCopyCDStringToClientFunc(copyCDStringToClient);
    TwAddVarRO(controlBar, "__currFilter", TW_TYPE_CDSTRING, &currentFilterStr,
            " label='Current Filter:' ");

    TwAddVarRO(controlBar, "__currEffect", TW_TYPE_CDSTRING, &currentEffectStr,
            " label='Current Effect:' ");

    TwAddVarRO(controlBar, "__TIME", TW_TYPE_FLOAT, &totalTime,
            " label='Time' ");

    TwAddVarRW(controlBar, "__Pause", TW_TYPE_BOOLCPP, &(paused), "label='Pause' ");

    TwAddVarRW(controlBar, "__Speed", TW_TYPE_FLOAT, &speed, " label='Speed' min=0.1 max=100.0 step=0.1 ");

    TwAddButton(controlBar, "__ResetTime", resetTimeCallback,
            this, "label='Reset Time' ");

    TwAddButton(controlBar, "__ResetCurrFilter", resetFilterCallback,
            this, "label='Reset Filter' ");

    TwAddButton(controlBar, "__ResetCurr", resetEffectCallback,
            this, "label='Reset Effect' ");

    if (!addFilters()) {
        LOG_ERROR(logger, "ControlPanel::addFilters() FAILED!!");
        return false;
    }


    if (!addEffects()) {
        LOG_ERROR(logger, "ControlPanel::addEffects() FAILED!!");
        return false;
    }

    struct {
        int x;
        int y;
    } controlPos = {CONTROL_TOOLBAR_POS_X, CONTROL_TOOLBAR_POS_Y};
    TwSetParam(controlBar, NULL, "position", TW_PARAM_INT32, 2, &controlPos);

    struct {
        int x, y;
    } controlSize = {CONTROL_TOOLBAR_WIDTH, CONTROL_TOOLBAR_HEIGHT};
    TwSetParam(controlBar, NULL, "size", TW_PARAM_INT32, 2, &controlSize);

    struct {
        int x;
        int y;
    } filterPos = {FILTER_TOOLBAR_POS_X, FILTER_TOOLBAR_POS_Y};
    TwSetParam(filterBar, NULL, "position", TW_PARAM_INT32, 2, &filterPos);

    struct {
        int x, y;
    } filterSize = {FILTER_TOOLBAR_WIDTH, FILTER_TOOLBAR_HEIGHT};
    TwSetParam(filterBar, NULL, "size", TW_PARAM_INT32, 2, &filterSize);

    struct {
        int x;
        int y;
    } effectPos = {EFFECT_TOOLBAR_POS_X, EFFECT_TOOLBAR_POS_Y};
    TwSetParam(effectBar, NULL, "position", TW_PARAM_INT32, 2, &effectPos);

    struct {
        int x, y;
    } effectSize = {EFFECT_TOOLBAR_WIDTH, EFFECT_TOOLBAR_HEIGHT};
    TwSetParam(effectBar, NULL, "size", TW_PARAM_INT32, 2, &effectSize);





    return true;
}

void ControlPanel::setAALevel(int aaLevel) {
    this->aaLevel = aaLevel;

    if (currentEffect->isAntiAliasingOn()) {
        currentEffect->program->use();
        currentEffect->program->setUniform("AALevel", aaLevel);
        currentEffect->program->setUniform("InvAALevel", 1.0f / (float) aaLevel);
    }
}

void ControlPanel::setAAScale(float aaScale) {
    this->aaScale = aaScale;
    if (currentEffect->isAntiAliasingOn()) {
        currentEffect->program->use();
        currentEffect->program->setUniform("AAScale", aaScale);
    }
}

void ControlPanel::updateCallback(float msTime) {
    if (paused) {
        return;
    }
    totalTime += msTime * speed;
    currentEffect->setTime(totalTime);
    refresh();
}

void ControlPanel::mousePosCallback(int x, int y) {
    TwEventMousePosGLFW(x, y);

    if (x < widthOffset) {
        mouseOver = true;
    } else {
        mouseOver = false;
    }

    if (!mouseOver && lmbPressed && x >= widthOffset && x < windowWidth
            && y >= 0 && y < windowHeight) {

        currentEffect->mouseDragged(x - widthOffset, imageHeight - y);
        currentFilter->mouseDragged(x - widthOffset, imageHeight - y);
    }
}

void ControlPanel::mouseButtonCallback(int id, int state) {
    TwGetParam(effectBar, NULL, "size", TW_PARAM_INT32, 2, &settingsBarBounds.width);
    TwGetParam(effectBar, NULL, "position", TW_PARAM_INT32, 2, &settingsBarBounds.x);

    if (id == GLFW_MOUSE_BUTTON_LEFT) {
        int x, y;
        glfwGetMousePos(&x, &y);

        if (state == GLFW_PRESS) {

            if (!mouseOver && x >= widthOffset && x < windowWidth
                    && y >= 0 && y < windowHeight) {
                currentEffect->mousePressed(x - widthOffset, imageHeight - y);
                currentFilter->mousePressed(x - widthOffset, imageHeight - y);
            }

            lmbPressed = true;
        } else {
            if (lmbPressed) {
                currentEffect->mouseReleased(x - widthOffset, imageHeight - y);
                currentFilter->mouseReleased(x - widthOffset, imageHeight - y);
            }

            lmbPressed = false;
            mouseOver = false;


        }
    }

    TwEventMouseButtonGLFW(id, state);
}

void ControlPanel::resizeCallback(int width, int height) {
    this->windowWidth = width;
    this->windowHeight = height;
    this->imageWidth = (int) ((float) width * (X_MAX - X_MIN) / 2.0f);
    this->imageHeight = height;
    this->widthOffset = windowWidth - imageWidth;

    LOG_TRACE(logger, "resizeCallback(" << this->imageWidth << ", " << this->imageHeight << ")");

    if (imageWidth == 0 || imageHeight == 0) {
        return;
    }

    if (!effectRenderTarget.create(this->imageWidth, this->imageHeight)) {
        LOG_ERROR(logger, "effectRenderTarget.create() FAILED!!");
    }

    if (!texRenderTarget.create(this->imageWidth, this->imageHeight)) {
        LOG_ERROR(logger, "texRenderTarget.create() FAILED!!");
    }

    if (!secondTexRenderTarget.create(this->imageWidth, this->imageHeight)) {
        LOG_ERROR(logger, "secondTexRenderTarget.create() FAILED!!");
    }

    texRenderTarget.getCurrentTexture().setSampler(
            REV_MIN_FILTER, REV_MAG_FILTER, REV_WRAP_S, REV_WRAP_T);

    secondTexRenderTarget.getCurrentTexture().setSampler(
            REV_MIN_FILTER, REV_MAG_FILTER, REV_WRAP_S, REV_WRAP_T);

    effectRenderTarget.getCurrentTexture().setSampler(
            REV_MIN_FILTER, REV_MAG_FILTER, REV_WRAP_S, REV_WRAP_T);

    if (currentEffect) {
        currentEffect->resize(this->imageWidth, this->imageHeight);
    }

    if (currentFilter) {
        currentFilter->resize(this->imageWidth, this->imageHeight);
    }

    TwWindowSize(width, height);
}

bool ControlPanel::addFilters() {
    AbstractFilter* filter = new NoFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "NoFilter::init() FAILED!!");
        delete filter;
    } else {
        filter->use();
        filterMap["brightnessContrast"] = filter;
    }
    glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new GaussianBlurFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "GaussianBlurFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["gaussianBlur"] = filter;
    }
    glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new TriangleBlurFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "TriangleBlurFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["triangleBlur"] = filter;
    }
    glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new ZoomBlurFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "ZoomBlurFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["zoomBlur"] = filter;
    }
    glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new TiltShiftFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "TiltShiftFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["tiltShift"] = filter;
    }
    glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new HexagonalPixelateFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "HexagonalPixelateFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["hexagonalPixelate"] = filter;
    }
    glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new LensBlurFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "LensBlurFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["edgeWork"] = filter;
    }
    glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new CameraFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "CameraFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["camera"] = filter;
    }
    glAlwaysAssertNoExit;
    //----------------------------------------------------
    filter = new BloomFilter(this);
    if (!filter->init()) {
        LOG_ERROR(logger, "BloomFilter::init() FAILED!!");
        delete filter;
    } else {
        filterMap["bloom"] = filter;
    }
    //----------------------------------------------------
    glAlwaysAssertNoExit;
    if (filterMap.empty()) {
        return false;
    }

    return true;
}

bool ControlPanel::addEffects() {
    //----------------------------------------------------
    AbstractEffect* effect = new TheRoadOfRibbonEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "TheRoadOfRibbonEffect::init() FAILED!!");
        delete effect;
    } else {
        effect->use();
        effectMap["theRoadOfRibbon"] = effect;
    }
    //----------------------------------------------------
    //    effect = new ReliefTunnelEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "ReliefTunnelEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["reliefTunnel"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new SquareTunnelEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "SquareTunnelEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["squareTunnel"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new MonjoriEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "MonjoriEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["monjori"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new KaleidoscopeEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "KaleidoscopeEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["kaleidoscope"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new DeformEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "DeformEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["deform"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new FlyEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "FlyEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["fly"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new PlasmaEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "PlasmaEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["plasma"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new PulseEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "PulseEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["pulse"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new WaterEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "WaterEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["water"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new ShapesEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "ShapesEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["shapes"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new MetablobEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "MetablobEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["metablob"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new SevenZeroFourEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "SevenZeroFourEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["sevenZeroFour"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new LandscapeEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "LandscapeEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["landscape"] = effect;
    //    }
    //    //----------------------------------------------------
    //    effect = new AppleEffect(this);
    //    if (!effect->init()) {
    //        LOG_ERROR(logger, "AppleEffect::init() FAILED!!");
    //        delete effect;
    //    } else {
    //        effectMap["apple"] = effect;
    //    }
    effect = new NautilusEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "NautilusEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["nautilus"] = effect;
    }
    //        //----------------------------------------------------
    //        effect = new MengerSpongeEffect(this);
    //        if (!effect->init()) {
    //            LOG_ERROR(logger, "MengerSpongeEffect::init() FAILED!!");
    //            delete effect;
    //        } else {
    //            effectMap["mengerSponge"] = effect;
    //        }
    //----------------------------------------------------
    effect = new ClodEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "ClodEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["clod"] = effect;
    }
    //----------------------------------------------------
    effect = new SlisesixEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "SlisesixEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["slisesix"] = effect;
    }
    //----------------------------------------------------
    effect = new SultEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "SultEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["sult"] = effect;
    }
    //        //----------------------------------------------------
    //        effect = new ValleyballEffect(this);
    //        if (!effect->init()) {
    //            LOG_ERROR(logger, "ValleyballEffect::init() FAILED!!");
    //            delete effect;
    //        } else {
    //            effectMap["valleyball"] = effect;
    //        }
    //----------------------------------------------------
    effect = new RedEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "RedEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["red"] = effect;
    }
    //----------------------------------------------------
    effect = new QuaternionEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "QuaternionEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["quaternion"] = effect;
    }
    //----------------------------------------------------
    effect = new MetaTunnelEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "MetaTunnelEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["metaTunnel"] = effect;
    }
    //        //----------------------------------------------------
    //        effect = new DroidEffect(this);
    //        if (!effect->init()) {
    //            LOG_ERROR(logger, "DroidEffect::init() FAILED!!");
    //            delete effect;
    //        } else {
    //            effectMap["droid"] = effect;
    //        }
    //----------------------------------------------------
    effect = new LeizexEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "LeizexEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["leizex"] = effect;
    }
    //----------------------------------------------------
    effect = new KinderpainterEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "KinderpainterEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["kinderpainter"] = effect;
    }
    //----------------------------------------------------
    effect = new MandelbulbEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "MandelbulbEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["mandelbulb"] = effect;
    }
    //----------------------------------------------------
    effect = new DiscoEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "DiscoEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["disco"] = effect;
    }
    //----------------------------------------------------

    if (effectMap.empty()) {
        return false;
    }

    return true;
}

void ControlPanel::bindTexture() {
    //texture.bindTexture();
    effectRenderTarget.getCurrentTexture().bind();
}

void ControlPanel::render() {
    if (!currentEffect || texture.isInitialized() == false) {
        TwDraw();
        return;
    }

    effectRenderTarget.use();
    glViewport(0, 0, imageWidth, imageHeight);

    glActiveTexture(GL_TEXTURE0);
    texture.bind();

    currentEffect->apply();
    secondQuadVBO.render();

    effectRenderTarget.unuse();
    glViewport(0, 0, windowWidth, windowHeight);

    glActiveTexture(GL_TEXTURE0);
    bindTexture();

    currentFilter->apply();
    quadVBO.render();

    TwDraw();
}

bool ControlPanel::loadBackgroundImages() {
    if (!getImagePathList("images/", backgroudImagesVector)) {
        return false;
    }

    if (backgroudImagesVector.size() == 0) {
        LOG_ERROR(logger, "There are no image files in 'images' direcotry!!");
        return false;
    }

    size_t size = backgroudImagesVector.size();

    TwEnumVal* imagesEnum = new TwEnumVal[size];
    string** filenames = new string*[size];
    int i = 0;

    for (vector<path>::iterator iter = backgroudImagesVector.begin();
            iter != backgroudImagesVector.end();) {
        path p = *(iter++);

        imagesEnum[i].Value = i;


        filenames[i] = new string(pathToString(p));
        imagesEnum[i].Label = filenames[i]->c_str();
        i++;
    }


    TwType imagesType = TwDefineEnum("BackgroundType", imagesEnum, size);
    TwAddVarCB(controlBar, "Background", imagesType, setBackgroundCallback,
            getBackgroundCallback, this, "label='Image:' ");

    for (int i = 0; i < size; i++) {
        delete filenames[i];
    }

    delete[] imagesEnum;
    delete[] filenames;

    string filename = pathToString(backgroudImagesVector[0]); //.file_string();

    if (!changeTexture(filename.c_str())) {
        return false;
    }

    return true;
}

bool ControlPanel::getImagePathList(const char* directory, vector<path>& result) {
    path p(directory);

    static string extensions[] = {".bmp", ".tga", ".png", ".jpg", ".jpeg"};
    static size_t extensionsSize = sizeof (extensions) / sizeof (string);

    try {
        if (exists(p)) {

            if (is_directory(p)) {

                for (directory_iterator iter = directory_iterator(p);
                        iter != directory_iterator();) {
                    path p1 = *(iter++);

                    if (!is_regular_file(p1)) {
                        continue;
                    }

                    for (int i = 0; i < extensionsSize; i++) {
                        path extPath = p1.extension();
                        const wchar_t* wExt = extPath.c_str();
                        string ext = StringUtil::fromWideString(wExt);
                        const char* ext1 = extensions[i].c_str();
                        const char* ext2 = ext.c_str();
                        if (strcmp(ext1, ext2) == 0) {
                            result.push_back(p1);
                            break;
                        }
                    }
                }
            } else {
                LOG_ERROR(logger, "'" << p << "' is not a directory!!");
                return false;
            }

        } else {
            LOG_ERROR(logger, "'" << p << "' does not exist!!");
            return false;
        }
    } catch (const filesystem_error& ex) {
        LOG_ERROR(logger, ex.what());
        return false;
    }

    return true;
}

void TW_CALL ControlPanel::setBackgroundCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->selectedBackgroundIndex = *(const int*) value;
    string filename = pathToString(controlPanel->backgroudImagesVector[controlPanel->selectedBackgroundIndex]);

    controlPanel->changeTexture(filename.c_str());
}

void TW_CALL ControlPanel::getBackgroundCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(int*) value = controlPanel->selectedBackgroundIndex;
}

void TW_CALL ControlPanel::resetEffectCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    LOG_TRACE(logger, "Reset current effect ");

    if (controlPanel->currentEffect != NULL) {
        controlPanel->currentEffect->reset();
    }
}

void TW_CALL ControlPanel::resetFilterCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    LOG_TRACE(logger, "Reset current filter ");

    if (controlPanel->currentFilter != NULL) {
        controlPanel->currentFilter->reset();
    }
}

bool ControlPanel::changeTexture(const char* textureFile) {

    texture.load(textureFile);

    if (texture.isInitialized() == false) {
        LOG_ERROR(logger, "Loading texture '" << textureFile << "' FAILED!");
        return false;
    }

    LOG_INFO(logger, "Texture '" << textureFile << "' loaded successfully.");

    return true;
}

// ---------------------------------------------------------------------------
// 2) Callback functions for C-Dynamic string variables
// ---------------------------------------------------------------------------

// Function called to copy the content of a C-Dynamic String (src) handled by
// the AntTweakBar library to a C-Dynamic string (*destPtr) handled by our application

void TW_CALL ControlPanel::copyCDStringToClient(char** destPtr, const char* src) {
    size_t srcLen = (src != NULL) ? strlen(src) : 0;
    size_t destLen = (*destPtr != NULL) ? strlen(*destPtr) : 0;

    // Alloc or realloc dest memory block if needed
    if (*destPtr == NULL)
        *destPtr = (char *) malloc(srcLen + 1);
    else if (srcLen > destLen)
        *destPtr = (char *) realloc(*destPtr, srcLen + 1);

    // Copy src
    if (srcLen > 0)
        strncpy(*destPtr, src, srcLen);
    (*destPtr)[srcLen] = '\0'; // null-terminated string
}

void ControlPanel::setEffect(AbstractEffect* effect) {
    this->currentEffect = effect;
    copyCDStringToClient(&currentEffectStr, effect->getName());

    LOG_INFO(logger, "Using effect: " << currentEffect->getName());

    setAAScale(aaScale);
    setAALevel(aaLevel);

    aaOn = this->currentEffect->isAntiAliasingOn();
    if (!aaOn) {
        TwDefine(" Control/__AALevel readonly=true ");
        TwDefine(" Control/__AAScale  readonly=true ");
    } else {
        TwDefine(" Control/__AALevel readonly=false ");
        TwDefine(" Control/__AAScale readonly=false ");
    }

    TwRefreshBar(controlBar);
}

void ControlPanel::setFilter(AbstractFilter* filter) {
    this->currentFilter = filter;
    copyCDStringToClient(&currentFilterStr, filter->getName());

    LOG_INFO(logger, "Using filter: " << currentFilter->getName());

    TwRefreshBar(controlBar);
}

void TW_CALL ControlPanel::setAALevelCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->setAALevel(*(const int*) value);
}

void TW_CALL ControlPanel::getAALevelCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(int*) value = controlPanel->getAALevel();
}

void TW_CALL ControlPanel::setAAScaleCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->setAAScale(*(const float*) value);
}

void TW_CALL ControlPanel::getAAScaleCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->getAAScale();
}

void TW_CALL ControlPanel::resetTimeCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    controlPanel->totalTime = 0.0f;

    controlPanel->currentEffect->setTime(controlPanel->totalTime);
    controlPanel->refresh();
}

