/* 
 * File:   TriangleBlurFilter.cpp
 * Author: Revers
 * 
 * Created on 28 maj 2012, 07:52
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "TriangleBlurFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Triangle Blur"
#define SHADER_FILE "shaders/TriangleBlurFilter.glsl"

Logger TriangleBlurFilter::logger = Logger::getInstance("filter.TriangleBlur");

#define DEFAULT_RADIUS 0.1f

TriangleBlurFilter::TriangleBlurFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    radius = DEFAULT_RADIUS;
}

bool TriangleBlurFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    //(controlPanel->imageWidth, controlPanel->imageHeight);

    TwAddVarCB(filterBar, "tri_radius", TW_TYPE_FLOAT,
            setRadiusCallback, getRadiusCallback, this,
            "label='Radius:' min=0.1 max=200.0 step=0.1 group='" FILTER_GROUP "' ");

    addLoadButton("tri_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void TriangleBlurFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void TriangleBlurFilter::reset() {
    if (!program) {
        return;
    }

    radius = DEFAULT_RADIUS;

    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

void TriangleBlurFilter::setRadius(float radius) {
    this->radius = radius;
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

void TriangleBlurFilter::apply() {
    program->use();

    glViewport(0, 0, controlPanel->imageWidth, controlPanel->imageHeight);

    program->setUniform("delta", deltaX);
    pass1();
    program->setUniform("delta", deltaY);
    pass2();

    glViewport(0, 0, controlPanel->windowWidth, controlPanel->windowHeight);
}

void TriangleBlurFilter::pass1() {
    controlPanel->texRenderTarget.use();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->bindTexture();

    glClear(GL_COLOR_BUFFER_BIT);

    controlPanel->secondQuadVBO.render();
}

void TriangleBlurFilter::pass2() {
    controlPanel->texRenderTarget.unuse();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->texRenderTarget.getCurrentTexture().bind();
}

void TriangleBlurFilter::resize(int width, int height) {
    deltaX = glm::vec2(radius / width, 0.0f);
    deltaY = glm::vec2(0.0f, radius / height);
}

const char* TriangleBlurFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
TriangleBlurFilter::setRadiusCallback(const void* value, void* clientData) {
    TriangleBlurFilter* filter = static_cast<TriangleBlurFilter*> (clientData);
    filter->radius = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->resize(filter->controlPanel->imageWidth,
            filter->controlPanel->imageHeight);
}

void TW_CALL
TriangleBlurFilter::getRadiusCallback(void* value, void* clientData) {
    TriangleBlurFilter* filter = static_cast<TriangleBlurFilter*> (clientData);

    *(float*) value = filter->radius;
}
