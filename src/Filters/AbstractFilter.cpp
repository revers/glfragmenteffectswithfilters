/* 
 * File:   IFilter.h
 * Author: Revers
 *
 * Created on 22 maj 2012, 21:50
 */

#include <iostream>
#include <string>
#include <string.h>


#include "../ControlPanel.h"

#include "AbstractFilter.h"

//#include <log4cplus/loggingmacros.h>
//#include <log4cplus/logger.h>



using namespace rev;
using namespace std;

AbstractFilter::AbstractFilter(ControlPanel* controlPanel_) : controlPanel(controlPanel_) {
    program = GLSLProgramPtr();
    filterBar = controlPanel->filterBar;
}

void AbstractFilter::addLoadButton(const char* twName, const char* twGroup) {
    std::string params = "label='Load Filter' group='";
    params += twGroup;
    params += "' ";
    TwAddButton(filterBar, twName, loadButtonCallback,
            this, params.c_str());
}

void AbstractFilter::use() {
    cout << "AbstractFilter::use()" << endl;
    controlPanel->setFilter(this);
}

void TW_CALL AbstractFilter::loadButtonCallback(void* clientData) {
    AbstractFilter* filter = static_cast<AbstractFilter*> (clientData);
    filter->use();
}

bool AbstractFilter::amILoaded() {
    return strcmp(getName(), controlPanel->getFilter()->getName()) == 0;
}

void AbstractFilter::apply() {
    glActiveTexture(GL_TEXTURE0);
    controlPanel->bindTexture();
    program->use();
}

