/* 
 * File:   LensBlurFilter.h
 * Author: Revers
 *
 * Created on 13 czerwiec 2012, 21:55
 */

#ifndef LENSBLURFILTER_H
#define	LENSBLURFILTER_H

#include "AbstractFilter.h"
#include <rev/gl/RevGLTextureRenderTarget.h>

namespace log4cplus {
    class Logger;
};

class LensBlurFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float radius;
    float angle;
    float brightness;
    float power;
    glm::vec2 dir[3];
    GLuint pass1Index;
    GLuint pass2Index;
    GLuint pass3Index;
    GLuint pass4Index;
    
    int lastWidth;
    int lastHeight;

    rev::GLTextureRenderTarget extraRenderTarget;
public:
    LensBlurFilter(ControlPanel* controlPanel);

    virtual ~LensBlurFilter() {
    }

    virtual bool init();
    virtual void apply();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();
private:
    
    void calcPower();
    void calcDir();
    
    void pass1();
    void pass2();
    void pass3();
    void pass4();
    void pass5();
    
    static void TW_CALL setRadiusCallback(const void* value, void* clientData);
    static void TW_CALL getRadiusCallback(void* value, void* clientData);
    
    static void TW_CALL setAngleCallback(const void* value, void* clientData);
    static void TW_CALL getAngleCallback(void* value, void* clientData);
    
    static void TW_CALL setBrightnessCallback(const void* value, void* clientData);
    static void TW_CALL getBrightnessCallback(void* value, void* clientData);
};

#endif	/* LENSBLURFILTER_H */

