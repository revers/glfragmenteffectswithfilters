/* 
 * File:   IFilter.h
 * Author: Revers
 *
 * Created on 22 maj 2012, 21:50
 */

#ifndef ABSTRACT_FILTER_H
#define	ABSTRACT_FILTER_H

#include "../LoggingDefines.h"
#include <rev/gl/RevGLSLProgram.h>

#include <AntTweakBar.h>
#include <boost/shared_ptr.hpp>

class ControlPanel;

typedef boost::shared_ptr<rev::GLSLProgram> GLSLProgramPtr;
class AbstractFilter {
    friend class ControlPanel;
protected:
    //typedef boost::shared_ptr<GLSLProgram> GLSLProgramPtr;
    ControlPanel* controlPanel;
    TwBar* filterBar;
    GLSLProgramPtr program;

public:
    AbstractFilter(ControlPanel* controlPanel_);

    virtual ~AbstractFilter() {
    }

    virtual bool init() = 0;
    virtual void reset() = 0;
    virtual const char* getName() = 0;
    virtual void use();
    virtual void apply();
    
    virtual void resize(int width, int height) {
    }
    
    virtual void mousePressed(int x, int y) {
    }
    
    virtual void mouseReleased(int x, int y) {
    }
    
    virtual void mouseDragged(int x, int y) {
    }
    
    virtual void mouseWheelCallback(int diff) {
    }

protected:
    void addLoadButton(const char* twName, const char* twGroup);

    bool amILoaded();
private:
    static void TW_CALL loadButtonCallback(void* clientData);
};


#endif	/* ABSTRACT_FILTER_H */

