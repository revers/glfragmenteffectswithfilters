/* 
 * File:   ZoomBlurFilter.h
 * Author: Kamil
 *
 * Created on 5 czerwiec 2012, 09:52
 */

#ifndef ZOOMBLURFILTER_H
#define	ZOOMBLURFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class ZoomBlurFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float strength;
    glm::vec2 texSize;
    glm::vec2 center;
    float centerRadiusSqr;
    
    bool drawCenter;
    bool dragMode;
    
    int lastMouseX;
    int lastMouseY;
    float oldWidth;
    float oldHeight;
public:
    ZoomBlurFilter(ControlPanel* controlPanel);

    virtual ~ZoomBlurFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();
    
    virtual void mousePressed(int x, int y);
    virtual void mouseReleased(int x, int y);
    virtual void mouseDragged(int x, int y);
    virtual void mouseWheelCallback(int diff);
private:
    static void TW_CALL setStrengthCallback(const void* value, void* clientData);
    static void TW_CALL getStrengthCallback(void* value, void* clientData);
    
    static void TW_CALL setDrawCenterCallback(const void* value, void* clientData);
    static void TW_CALL getDrawCenterCallback(void* value, void* clientData);
};

#endif	/* ZOOMBLURFILTER_H */

