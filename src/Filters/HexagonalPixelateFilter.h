/* 
 * File:   HexagonalPixelateFilter.h
 * Author: Revers
 *
 * Created on 6 czerwiec 2012, 21:30
 */

#ifndef HEXAGONALPIXELATEFILTER_H
#define	HEXAGONALPIXELATEFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class HexagonalPixelateFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float scale;
    glm::vec2 texSize;
    glm::vec2 center;
    float centerRadiusSqr;

    bool drawCenter;
    bool dragMode;

    int lastMouseX;
    int lastMouseY;
    float oldWidth;
    float oldHeight;
public:
    HexagonalPixelateFilter(ControlPanel* controlPanel);

    virtual ~HexagonalPixelateFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();

    virtual void mousePressed(int x, int y);
    virtual void mouseReleased(int x, int y);
    virtual void mouseDragged(int x, int y);
    virtual void mouseWheelCallback(int diff);
private:
    static void TW_CALL setScaleCallback(const void* value, void* clientData);
    static void TW_CALL getScaleCallback(void* value, void* clientData);

    static void TW_CALL setDrawCenterCallback(const void* value, void* clientData);
    static void TW_CALL getDrawCenterCallback(void* value, void* clientData);
};

#endif	/* HEXAGONALPIXELATEFILTER_H */

