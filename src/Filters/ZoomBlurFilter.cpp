/* 
 * File:   ZoomBlurFilter.cpp
 * Author: Kamil
 * 
 * Created on 5 czerwiec 2012, 09:52
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "ZoomBlurFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <iostream>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Zoom Blur"
#define SHADER_FILE "shaders/ZoomBlurFilter.glsl"

Logger ZoomBlurFilter::logger = Logger::getInstance("filter.ZoomBlur");

#define DEFAULT_STRENGTH 0.3f

ZoomBlurFilter::ZoomBlurFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    strength = DEFAULT_STRENGTH;
    centerRadiusSqr = 100.0f;
    drawCenter = true;
    lastMouseX = 0;
    lastMouseY = 0;
    oldWidth = -1.0f;
    oldHeight = -1.0f;
    dragMode = false;
}

bool ZoomBlurFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("strength", strength);
    program->setUniform("centerRadiusSqr", centerRadiusSqr);
    program->setUniform("drawCenter", drawCenter);

    TwAddVarCB(filterBar, "zbl_strength", TW_TYPE_FLOAT,
            setStrengthCallback, getStrengthCallback, this,
            "label='Strength:' min=0.0 max=100.0 step=0.01 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "zbl_drawCenter", TW_TYPE_BOOL32,
            setDrawCenterCallback, getDrawCenterCallback, this,
            "label='Draw Center:' group='" FILTER_GROUP "' ");

    addLoadButton("zbl_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void ZoomBlurFilter::mouseDragged(int x, int y) {
    
    if (dragMode) {
        int diffX = x - lastMouseX;
        int diffY = y - lastMouseY;
        center.x += (float) diffX;
        center.y += (float) diffY;

        program->use();
        program->setUniform("center", center);
    }

    lastMouseX = x;
    lastMouseY = y;
}

void ZoomBlurFilter::mouseReleased(int x, int y) {
    if (dragMode) {
        dragMode = false;
    }
}

void ZoomBlurFilter::mousePressed(int x, int y) {
    lastMouseX = x;
    lastMouseY = y;

    if (!drawCenter) {
        return;
    }

    float diffX = center.x - (float) x;
    float diffY = center.y - (float) y;

    if (diffX * diffX + diffY * diffY <= centerRadiusSqr) {
        dragMode = true;
    } else {
        dragMode = false;
    }
}

#define MOUSE_WHEEL_STEP 0.05f

void ZoomBlurFilter::mouseWheelCallback(int diff) {

    if (diff > 0) {
        strength -= MOUSE_WHEEL_STEP;
        if (strength < 0.0f) {
            strength = 0.0f;
        }
    } else if (diff < 0) {
        strength += MOUSE_WHEEL_STEP;
        if (strength > 100.0f) {
            strength = 100.0f;
        }
    }

    program->setUniform("strength", strength);
}

void ZoomBlurFilter::resize(int width, int height) {

    texSize.x = width;
    texSize.y = height;

    if (oldWidth < 0.0f) {
        center = texSize / 2.0f;
    } else {

        float prcX = center.x / oldWidth;
        float prcY = center.y / oldHeight;

        center.x = prcX * texSize.x;
        center.y = prcY * texSize.y;
    }

    oldWidth = (float) width;
    oldHeight = (float) height;

    program->use();

    program->setUniform("texSize", texSize);
    program->setUniform("center", center);
}

void ZoomBlurFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void ZoomBlurFilter::reset() {
    if (!program) {
        return;
    }

    strength = DEFAULT_STRENGTH;
    drawCenter = true;

    program->use();
    program->setUniform("strength", strength);
    program->setUniform("drawCenter", drawCenter);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* ZoomBlurFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
ZoomBlurFilter::setStrengthCallback(const void* value, void* clientData) {
    ZoomBlurFilter* filter = static_cast<ZoomBlurFilter*> (clientData);
    filter->strength = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("strength", filter->strength);
}

void TW_CALL
ZoomBlurFilter::getStrengthCallback(void* value, void* clientData) {
    ZoomBlurFilter* filter = static_cast<ZoomBlurFilter*> (clientData);

    *(float*) value = filter->strength;
}

void TW_CALL
ZoomBlurFilter::setDrawCenterCallback(const void* value, void* clientData) {
    ZoomBlurFilter* filter = static_cast<ZoomBlurFilter*> (clientData);
    filter->drawCenter = *(const bool*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("drawCenter", filter->drawCenter);
}

void TW_CALL
ZoomBlurFilter::getDrawCenterCallback(void* value, void* clientData) {
    ZoomBlurFilter* filter = static_cast<ZoomBlurFilter*> (clientData);

    *(bool*) value = filter->drawCenter;
}