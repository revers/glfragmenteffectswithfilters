/* 
 * File:   NoFilter.h
 * Author: Revers
 *
 * Created on 21 czerwiec 2012, 20:09
 */

#ifndef NOFILTER_H
#define	NOFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class NoFilter : public AbstractFilter {
    static log4cplus::Logger logger;
public:
    NoFilter(ControlPanel* controlPanel);

    virtual ~NoFilter() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
};

#endif	/* NOFILTER_H */

