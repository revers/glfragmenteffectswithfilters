/* 
 * File:   LensBlurFilter.cpp
 * Author: Revers
 * 
 * Created on 13 czerwiec 2012, 21:55
 */
#include "../ControlPanel.h"

#include "LensBlurFilter.h"

#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>
#include "LensBlurFilter.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include "../RenderTargetDefines.h"

using namespace rev;
using namespace log4cplus;
using namespace std;

#ifndef M_PI
#define M_PI 3.141592653589793
#endif

#define FILTER_GROUP "Lens Blur"
#define SHADER_FILE "shaders/LensBlurFilter.glsl"

Logger LensBlurFilter::logger = Logger::getInstance("filter.LensBlur");

#define DEFAULT_RADIUS 8.0f
#define DEFAULT_ANGLE 1.0f
#define DEFAULT_BRIGHTNESS 0.2f

LensBlurFilter::LensBlurFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel)
//extraRenderTarget(REV_MIN_FILTER, REV_MAG_FILTER, REV_WRAP_S, REV_WRAP_T) 
{

    radius = DEFAULT_RADIUS;
    angle = DEFAULT_ANGLE;
    brightness = DEFAULT_BRIGHTNESS;
    pass1Index = 0;
    pass2Index = 0;
}

bool LensBlurFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();

    program->setUniform("Tex1", 0);
    program->setUniform("Tex2", 1);
    program->setUniform("power", power);

    pass1Index = glGetSubroutineIndex(program->getHandle(),
            GL_FRAGMENT_SHADER, "pass1");
    if (glGetError() == GL_INVALID_VALUE) {
        LOG_ERROR(logger, "Cannot find pass1 subroutine!!");
        return false;
    }

    pass2Index = glGetSubroutineIndex(program->getHandle(),
            GL_FRAGMENT_SHADER, "pass2");
    if (glGetError() == GL_INVALID_VALUE) {
        LOG_ERROR(logger, "Cannot find pass2 subroutine!!");
        return false;
    }

    pass3Index = glGetSubroutineIndex(program->getHandle(),
            GL_FRAGMENT_SHADER, "pass3");
    if (glGetError() == GL_INVALID_VALUE) {
        LOG_ERROR(logger, "Cannot find pass3 subroutine!!");
        return false;
    }

    pass4Index = glGetSubroutineIndex(program->getHandle(),
            GL_FRAGMENT_SHADER, "pass4");
    if (glGetError() == GL_INVALID_VALUE) {
        LOG_ERROR(logger, "Cannot find pass4 subroutine!!");
        return false;
    }

    TwAddVarCB(filterBar, "lbl_radius", TW_TYPE_FLOAT,
            setRadiusCallback, getRadiusCallback, this,
            "label='Radius:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "lbl_angle", TW_TYPE_FLOAT,
            setAngleCallback, getAngleCallback, this,
            "label='Angle:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "lbl_brightness", TW_TYPE_FLOAT,
            setBrightnessCallback, getBrightnessCallback, this,
            "label='Brightness:' min=-1.0 max=1.0 step=0.01 group='" FILTER_GROUP "' ");

    addLoadButton("lbl_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void LensBlurFilter::resize(int width, int height) {

    if (width != lastWidth || height != lastHeight) {

        if (!extraRenderTarget.create(width, height)) {
            LOG_ERROR(logger, "extraRenderTarget.create() FAILED!!");
            return;
        }

        extraRenderTarget.getCurrentTexture().setSampler(
                REV_MIN_FILTER, REV_MAG_FILTER, REV_WRAP_S, REV_WRAP_T);

        lastWidth = width;
        lastHeight = height;
    }
}

void LensBlurFilter::apply() {
    program->use();
    program->setUniform("power", power);

    glViewport(0, 0, controlPanel->imageWidth, controlPanel->imageHeight);

    pass1();
    pass2();
    pass3();
    pass4();
    pass5();

    glViewport(0, 0, controlPanel->windowWidth, controlPanel->windowHeight);
}

void LensBlurFilter::pass1() {
    controlPanel->secondTexRenderTarget.use();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->bindTexture();

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass1Index);

    glClear(GL_COLOR_BUFFER_BIT);
    controlPanel->secondQuadVBO.render();
}

void LensBlurFilter::pass2() {
    program->setUniform("delta0", dir[0]);

    controlPanel->texRenderTarget.use();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->secondTexRenderTarget.getCurrentTexture().bind();

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass2Index);

    glClear(GL_COLOR_BUFFER_BIT);
    controlPanel->secondQuadVBO.render();
}

void LensBlurFilter::pass3() {
    program->setUniform("delta0", dir[1]);
    program->setUniform("delta1", dir[2]);

    extraRenderTarget.use();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->texRenderTarget.getCurrentTexture().bind();

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass3Index);

    glClear(GL_COLOR_BUFFER_BIT);
    controlPanel->secondQuadVBO.render();
}

void LensBlurFilter::pass4() {
    program->setUniform("delta1", dir[1]);

    controlPanel->texRenderTarget.use();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->secondTexRenderTarget.getCurrentTexture().bind();

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass2Index);

    glClear(GL_COLOR_BUFFER_BIT);
    controlPanel->secondQuadVBO.render();
}

void LensBlurFilter::pass5() {
    program->setUniform("delta0", dir[2]);
    program->setUniform("power", 1.0f / power);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glActiveTexture(GL_TEXTURE1);
    extraRenderTarget.getCurrentTexture().bind();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->texRenderTarget.getCurrentTexture().bind();

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass4Index);
}

void LensBlurFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);

    calcPower();
    calcDir();
}

void LensBlurFilter::reset() {
    if (!program) {
        return;
    }

    radius = DEFAULT_RADIUS;
    angle = DEFAULT_ANGLE;
    brightness = DEFAULT_BRIGHTNESS;

    calcPower();
    calcDir();

    program->use();
    program->setUniform("power", power);

    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* LensBlurFilter::getName() {
    return FILTER_GROUP;
}

void LensBlurFilter::calcPower() {
    float bright = brightness < -1.0f ? -1.0f : brightness > 1.0f ? 1.0 : brightness;
    power = pow(10.0, bright);

    program->use();
    program->setUniform("power", power);
}

void LensBlurFilter::calcDir() {
    for (int i = 0; i < 3; i++) {
        double a = angle + (double) i * M_PI * 2.0 / 3.0;
        dir[i] = glm::vec2((float) (radius * sin(a) / controlPanel->imageWidth),
                (float) (radius * cos(a) / controlPanel->imageHeight));
    }
}

void TW_CALL
LensBlurFilter::setRadiusCallback(const void* value, void* clientData) {
    LensBlurFilter* filter = static_cast<LensBlurFilter*> (clientData);
    filter->radius = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->calcDir();
}

void TW_CALL
LensBlurFilter::getRadiusCallback(void* value, void* clientData) {
    LensBlurFilter* filter = static_cast<LensBlurFilter*> (clientData);

    *(float*) value = filter->radius;
}

void TW_CALL
LensBlurFilter::setAngleCallback(const void* value, void* clientData) {
    LensBlurFilter* filter = static_cast<LensBlurFilter*> (clientData);
    filter->angle = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->calcDir();
}

void TW_CALL
LensBlurFilter::getAngleCallback(void* value, void* clientData) {
    LensBlurFilter* filter = static_cast<LensBlurFilter*> (clientData);

    *(float*) value = filter->angle;
}

void TW_CALL
LensBlurFilter::setBrightnessCallback(const void* value, void* clientData) {
    LensBlurFilter* filter = static_cast<LensBlurFilter*> (clientData);
    filter->brightness = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->calcPower();
}

void TW_CALL
LensBlurFilter::getBrightnessCallback(void* value, void* clientData) {
    LensBlurFilter* filter = static_cast<LensBlurFilter*> (clientData);

    *(float*) value = filter->brightness;
}