/* 
 * File:   GaussianBlurFilter.cpp
 * Author: Revers
 * 
 * Created on 27 maj 2012, 21:35
 */

#include "GaussianBlurFilter.h"

#include <GL/glew.h>
#include <GL/gl.h>
#include "GaussianBlurFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define PI 3.141592653589793

#define FILTER_GROUP "Gaussian Blur"
#define SHADER_FILE "shaders/GaussianBlurFilter.glsl"

Logger GaussianBlurFilter::logger = Logger::getInstance("filter.GaussianBlur");

#define DEFAULT_SIGMA2 8.0f
#define DEFAULT_OFFSET_FACTOR 1.0f

GaussianBlurFilter::GaussianBlurFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    sigma2 = DEFAULT_SIGMA2;
    offsetFactor = DEFAULT_OFFSET_FACTOR;
    pass1Index = 0;
    pass2Index = 0;
}

bool GaussianBlurFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("Tex1", 0);

    pass1Index = glGetSubroutineIndex(program->getHandle(), GL_FRAGMENT_SHADER, "pass1");
    if (glGetError() == GL_INVALID_VALUE) {
        LOG_ERROR(logger, "Cannot find pass1 subroutine!!");
        return false;
    }

    pass2Index = glGetSubroutineIndex(program->getHandle(), GL_FRAGMENT_SHADER, "pass2");
    if (glGetError() == GL_INVALID_VALUE) {
        LOG_ERROR(logger, "Cannot find pass2 subroutine!!");
        return false;
    }

    TwAddVarCB(filterBar, "gauss_sigma2", TW_TYPE_FLOAT,
            setSigma2Callback, getSigma2Callback, this,
            "label='Sigma^2:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "gauss_offset", TW_TYPE_FLOAT,
            setOffsetCallback, getOffsetCallback, this,
            "label='Pixel Offset:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    addLoadButton("gauss_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    setupGaussian();
    setupOffset();

    return true;
}

void GaussianBlurFilter::setupOffset() {
    char uniName[20];

    program->use();
    // Normalize the weights and set the uniform
    for (int i = 0; i < 5; i++) {
        snprintf(uniName, 20, "PixOffset[%d]", i);
        float val = offsetFactor * i;
        program->setUniform(uniName, val);
    }
}

void GaussianBlurFilter::setupGaussian() {

    char uniName[20];
    float weights[5];
    float sum;

    // Compute and sum the weights
    weights[0] = gauss(0, sigma2);
    sum = weights[0];
    for (int i = 1; i < 5; i++) {
        weights[i] = gauss(i, sigma2);
        sum += 2 * weights[i];
    }

    program->use();
    // Normalize the weights and set the uniform
    for (int i = 0; i < 5; i++) {
        snprintf(uniName, 20, "Weight[%d]", i);
        float val = weights[i] / sum;
        program->setUniform(uniName, val);
    }
}

float GaussianBlurFilter::gauss(float x, float sigma2) {
    double coeff = 1.0 / (2.0 * PI * sigma2);
    double expon = -(x * x) / (2.0 * sigma2);
    return (float) (coeff * exp(expon));
}

void GaussianBlurFilter::apply() {
    program->use();

    glViewport(0, 0, controlPanel->imageWidth, controlPanel->imageHeight);

    pass1();
    pass2();

    glViewport(0, 0, controlPanel->windowWidth, controlPanel->windowHeight);
}

void GaussianBlurFilter::pass1() {

    controlPanel->texRenderTarget.use();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->bindTexture();

    glClear(GL_COLOR_BUFFER_BIT);

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass1Index);

    controlPanel->secondQuadVBO.render();
}

void GaussianBlurFilter::pass2() {
    controlPanel->texRenderTarget.unuse();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->texRenderTarget.getCurrentTexture().bind();

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass2Index);
}

void GaussianBlurFilter::resize(int width, int height) {
    program->use();
    program->setUniform("Width", width);
    program->setUniform("Height", height);
}

void GaussianBlurFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void GaussianBlurFilter::reset() {
    if (!program) {
        return;
    }

    sigma2 = DEFAULT_SIGMA2;
    offsetFactor = DEFAULT_OFFSET_FACTOR;

    program->use();

    setupGaussian();
    setupOffset();
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* GaussianBlurFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
GaussianBlurFilter::setSigma2Callback(const void* value, void* clientData) {
    GaussianBlurFilter* filter = static_cast<GaussianBlurFilter*> (clientData);
    filter->sigma2 = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->setupGaussian();
    //  filter->program->setUniform("EdgeThreshold", filter->threshold);
}

void TW_CALL
GaussianBlurFilter::getSigma2Callback(void* value, void* clientData) {
    GaussianBlurFilter* filter = static_cast<GaussianBlurFilter*> (clientData);

    *(float*) value = filter->sigma2;
}

void TW_CALL
GaussianBlurFilter::setOffsetCallback(const void* value, void* clientData) {
    GaussianBlurFilter* filter = static_cast<GaussianBlurFilter*> (clientData);
    filter->offsetFactor = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->setupOffset();
}

void TW_CALL
GaussianBlurFilter::getOffsetCallback(void* value, void* clientData) {
    GaussianBlurFilter* filter = static_cast<GaussianBlurFilter*> (clientData);

    *(float*) value = filter->offsetFactor;
}