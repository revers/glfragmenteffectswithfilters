/* 
 * File:   NoFilter.cpp
 * Author: Revers
 * 
 * Created on 21 czerwiec 2012, 20:09
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include "NoFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define FILTER_GROUP "No Filter"
#define SHADER_FILE "shaders/NoFilter.glsl"

Logger NoFilter::logger = Logger::getInstance("filter.No");

NoFilter::NoFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
}

bool NoFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    addLoadButton("no_filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void NoFilter::reset() {
}

const char* NoFilter::getName() {
    return FILTER_GROUP;
}