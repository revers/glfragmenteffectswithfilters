/* 
 * File:   BloomFilter.h
 * Author: Revers
 *
 * Created on 16 czerwiec 2012, 11:23
 */

#ifndef BLOOMFILTER_H
#define	BLOOMFILTER_H

#include "AbstractFilter.h"

namespace log4cplus {
    class Logger;
};

class BloomFilter : public AbstractFilter {
    static log4cplus::Logger logger;
    float threshold;
    float sigma2;
    float offsetFactor;
    glm::vec2 texSize;
    GLuint pass1Index;
    GLuint pass2Index;
    GLuint pass3Index;
public:
    BloomFilter(ControlPanel* controlPanel);

    virtual ~BloomFilter() {
    }

    virtual bool init();
    virtual void apply();
    virtual void reset();
    virtual const char* getName();
    virtual void resize(int width, int height);
    virtual void use();
private:
    void pass1();
    void pass2();
    void pass3();
    
    void setupGaussian();
    float gauss(float x, float sigma2);
    void setupOffset();
    
    static void TW_CALL setSigma2Callback(const void* value, void* clientData);
    static void TW_CALL getSigma2Callback(void* value, void* clientData);
    
    static void TW_CALL setOffsetCallback(const void* value, void* clientData);
    static void TW_CALL getOffsetCallback(void* value, void* clientData);
    
    static void TW_CALL setThresholdCallback(const void* value, void* clientData);
    static void TW_CALL getThresholdCallback(void* value, void* clientData);
};

#endif	/* BLOOMFILTER_H */

