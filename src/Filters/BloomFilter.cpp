/* 
 * File:   BloomFilter.cpp
 * Author: Revers
 * 
 * Created on 16 czerwiec 2012, 11:23
 */

#include "BloomFilter.h"

#include <GL/glew.h>
#include <GL/gl.h>
#include "BloomFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define PI 3.141592653589793

#define FILTER_GROUP "Bloom"
#define SHADER_FILE "shaders/BloomFilter.glsl"

Logger BloomFilter::logger = Logger::getInstance("filter.Bloom");

#define DEFAULT_SIGMA2 8.0f
#define DEFAULT_OFFSET_FACTOR 15.0f
#define DEFAULT_THRESHOLD 0.4f

BloomFilter::BloomFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    threshold = DEFAULT_THRESHOLD;
    sigma2 = DEFAULT_SIGMA2;
    offsetFactor = DEFAULT_OFFSET_FACTOR;
    pass1Index = 0;
    pass2Index = 0;
    pass3Index = 0;
}

bool BloomFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    //program->setUniform("EdgeThreshold", threshold);
    program->setUniform("Tex1", 0);
    program->setUniform("Tex2", 1);

    //(controlPanel->imageWidth, controlPanel->imageHeight);

    TwAddVarCB(filterBar, "blm_sigma2", TW_TYPE_FLOAT,
            setSigma2Callback, getSigma2Callback, this,
            "label='Sigma^2:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "blm_offset", TW_TYPE_FLOAT,
            setOffsetCallback, getOffsetCallback, this,
            "label='Pixel Offset:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "blm_threshold", TW_TYPE_FLOAT,
            setThresholdCallback, getThresholdCallback, this,
            "label='Threshold:' min=0.0 max=1.0 step=0.01 group='" FILTER_GROUP "' ");

    addLoadButton("blm_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    pass1Index = glGetSubroutineIndex(program->getHandle(), GL_FRAGMENT_SHADER, "pass1");
    pass2Index = glGetSubroutineIndex(program->getHandle(), GL_FRAGMENT_SHADER, "pass2");
    pass3Index = glGetSubroutineIndex(program->getHandle(), GL_FRAGMENT_SHADER, "pass3");
    setupGaussian();
    setupOffset();

    return true;
}

void BloomFilter::setupOffset() {
    char uniName[20];

    program->use();
    // Normalize the weights and set the uniform
    for (int i = 0; i < 5; i++) {
        snprintf(uniName, 20, "PixOffset[%d]", i);
        float val = offsetFactor * i;
        program->setUniform(uniName, val);
    }
}

void BloomFilter::setupGaussian() {

    char uniName[20];
    float weights[5];
    float sum;

    // Compute and sum the weights
    weights[0] = gauss(0, sigma2);
    sum = weights[0];
    for (int i = 1; i < 5; i++) {
        weights[i] = gauss(i, sigma2);
        sum += 2 * weights[i];
    }

    program->use();
    // Normalize the weights and set the uniform
    for (int i = 0; i < 5; i++) {
        snprintf(uniName, 20, "Weight[%d]", i);
        float val = weights[i] / sum;
        program->setUniform(uniName, val);
    }
}

float BloomFilter::gauss(float x, float sigma2) {
    double coeff = 1.0 / (2.0 * PI * sigma2);
    double expon = -(x * x) / (2.0 * sigma2);
    return (float) (coeff * exp(expon));
}

void BloomFilter::apply() {
    program->use();

    glViewport(0, 0, controlPanel->imageWidth, controlPanel->imageHeight);

    pass1();
    pass2();
    pass3();

    glViewport(0, 0, controlPanel->windowWidth, controlPanel->windowHeight);
}

void BloomFilter::pass1() {
    controlPanel->texRenderTarget.use();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->bindTexture();

    glClear(GL_COLOR_BUFFER_BIT);

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass1Index);

    controlPanel->secondQuadVBO.render();
}

void BloomFilter::pass2() {
    controlPanel->secondTexRenderTarget.use();

    glActiveTexture(GL_TEXTURE1);
    controlPanel->texRenderTarget.getCurrentTexture().bind();

    glClear(GL_COLOR_BUFFER_BIT);

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass2Index);

    controlPanel->secondQuadVBO.render();
}

void BloomFilter::pass3() {
    controlPanel->secondTexRenderTarget.unuse();

    glActiveTexture(GL_TEXTURE1);
    controlPanel->secondTexRenderTarget.getCurrentTexture().bind();
    
    glActiveTexture(GL_TEXTURE0);
    controlPanel->bindTexture();

    glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1, &pass3Index);
}

void BloomFilter::resize(int width, int height) {
    program->use();
    program->setUniform("Width", width);
    program->setUniform("Height", height);
}

void BloomFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void BloomFilter::reset() {
    if (!program) {
        return;
    }

    threshold = DEFAULT_THRESHOLD;
    sigma2 = DEFAULT_SIGMA2;
    offsetFactor = DEFAULT_OFFSET_FACTOR;

    program->use();

    program->setUniform("LumThresh", threshold);

    setupGaussian();
    setupOffset();
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* BloomFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
BloomFilter::setSigma2Callback(const void* value, void* clientData) {
    BloomFilter* filter = static_cast<BloomFilter*> (clientData);
    filter->sigma2 = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->setupGaussian();
    //  filter->program->setUniform("EdgeThreshold", filter->threshold);
}

void TW_CALL
BloomFilter::getSigma2Callback(void* value, void* clientData) {
    BloomFilter* filter = static_cast<BloomFilter*> (clientData);

    *(float*) value = filter->sigma2;
}

void TW_CALL
BloomFilter::setOffsetCallback(const void* value, void* clientData) {
    BloomFilter* filter = static_cast<BloomFilter*> (clientData);
    filter->offsetFactor = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->setupOffset();
}

void TW_CALL
BloomFilter::getOffsetCallback(void* value, void* clientData) {
    BloomFilter* filter = static_cast<BloomFilter*> (clientData);

    *(float*) value = filter->offsetFactor;
}

void TW_CALL
BloomFilter::setThresholdCallback(const void* value, void* clientData) {
    BloomFilter* filter = static_cast<BloomFilter*> (clientData);
    filter->threshold = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("LumThresh", filter->threshold);
}

void TW_CALL
BloomFilter::getThresholdCallback(void* value, void* clientData) {
    BloomFilter* filter = static_cast<BloomFilter*> (clientData);

    *(float*) value = filter->threshold;
}