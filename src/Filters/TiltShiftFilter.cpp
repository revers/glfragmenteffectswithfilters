/* 
 * File:   TiltShiftFilter.cpp
 * Author: Kamil
 * 
 * Created on 6 czerwiec 2012, 09:13
 */

#include <GL/glew.h>
#include <GL/gl.h>
#include "TiltShiftFilter.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>
#include <iostream>
#include <glm/core/func_geometric.hpp>

using namespace rev;
using namespace log4cplus;
using namespace std;


#define FILTER_GROUP "Tilt Shift"
#define SHADER_FILE "shaders/TiltShiftFilter.glsl"

Logger TiltShiftFilter::logger = Logger::getInstance("filter.TiltShift");

#define DEFAULT_BLUR_RADIUS 15.0f
#define DEFAULT_GRADIENT_RADIUS 310.0f

TiltShiftFilter::TiltShiftFilter(ControlPanel* controlPanel)
: AbstractFilter(controlPanel) {
    blurRadius = DEFAULT_BLUR_RADIUS;
    gradientRadius = DEFAULT_GRADIENT_RADIUS;
    controlPointsRadiusSqr = 100.0f;
    drawControlPoints = true;
    lastMouseX = 0;
    lastMouseY = 0;
    oldWidth = -1.0f;
    oldHeight = -1.0f;
    beginDragMode = false;
    endDragMode = false;
}

bool TiltShiftFilter::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderGLSLFile(SHADER_FILE)) {
        LOG_ERROR(logger, "Compilation of file " SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking file " SHADER_FILE " FAILED!!");
        return false;
    }

    program->use();
    program->setUniform("blurRadius", blurRadius);
    program->setUniform("gradientRadius", gradientRadius);
    program->setUniform("controlPointsRadiusSqr", controlPointsRadiusSqr);
    program->setUniform("drawControlPoints", drawControlPoints);

    TwAddVarCB(filterBar, "tsh_blurRadius", TW_TYPE_FLOAT,
            setBlurRadiusCallback, getBlurRadiusCallback, this,
            "label='Blur Radius:' min=0.0 max=100.0 step=0.1 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "tsh_gradientRadius", TW_TYPE_FLOAT,
            setGradientRadiusCallback, getGradientRadiusCallback, this,
            "label='Gradient Radius:' min=0.0 max=1000.0 step=10.0 group='" FILTER_GROUP "' ");

    TwAddVarCB(filterBar, "tsh_drawControlPoints", TW_TYPE_BOOL32,
            setDrawCenterCallback, getDrawCenterCallback, this,
            "label='Control Points:' group='" FILTER_GROUP "' ");

    addLoadButton("tsh_Filter", FILTER_GROUP);

    int opened = 0;
    TwSetParam(filterBar, FILTER_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void TiltShiftFilter::mouseDragged(int x, int y) {
    //    LOG_TRACE(logger, "mouseDragged(" << x << ", " << y << ")"
    //            << "; center.x = " << center.x << "; center.y = " << center.y);

    int diffX = x - lastMouseX;
    int diffY = y - lastMouseY;

    if (beginDragMode) {
        controlPointBegin.x += (float) diffX;
        controlPointBegin.y += (float) diffY;

        program->use();
        program->setUniform("begin", controlPointBegin);
    } else if (endDragMode) {
        controlPointEnd.x += (float) diffX;
        controlPointEnd.y += (float) diffY;

        program->use();
        program->setUniform("end", controlPointEnd);
    }

    lastMouseX = x;
    lastMouseY = y;
}

void TiltShiftFilter::mousePressed(int x, int y) {
    //    LOG_TRACE(logger, "mousePressed(" << x << ", " << y << ") "
    //            << "; center.x = " << center.x << "; center.y = " << center.y);
    lastMouseX = x;
    lastMouseY = y;

    if (!drawControlPoints) {
        return;
    }

    float diffX = controlPointBegin.x - (float) x;
    float diffY = controlPointBegin.y - (float) y;

    if (diffX * diffX + diffY * diffY <= controlPointsRadiusSqr) {
        beginDragMode = true;
        return;
    } else {
        beginDragMode = false;
    }

    diffX = controlPointEnd.x - (float) x;
    diffY = controlPointEnd.y - (float) y;

    if (diffX * diffX + diffY * diffY <= controlPointsRadiusSqr) {
        endDragMode = true;
        return;
    } else {
        endDragMode = false;
    }
}

#define MOUSE_WHEEL_STEP 10.0f

void TiltShiftFilter::mouseWheelCallback(int diff) {

    if (diff > 0) {
        gradientRadius -= MOUSE_WHEEL_STEP;
        if (gradientRadius < 0.0f) {
            gradientRadius = 0.0f;
        }
    } else if (diff < 0) {
        gradientRadius += MOUSE_WHEEL_STEP;
        if (gradientRadius > 1000.0f) {
            gradientRadius = 1000.0f;
        }
    }

    program->setUniform("gradientRadius", gradientRadius);
}

void TiltShiftFilter::apply() {
    float dx = controlPointEnd.x - controlPointBegin.x;
    float dy = controlPointEnd.y - controlPointBegin.y;
    float d = sqrt(dx * dx + dy * dy);

    vec2 delta = vec2(dx / d, dy / d);

    program->use();
    program->setUniform("delta", delta);
    pass1();

    delta = vec2(-dy / d, dx / d);
    program->setUniform("delta", delta);
    pass2();
}

void TiltShiftFilter::pass1() {
    glViewport(0, 0, controlPanel->imageWidth, controlPanel->imageHeight);

    controlPanel->texRenderTarget.use();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->bindTexture();

    glClear(GL_COLOR_BUFFER_BIT);
    controlPanel->secondQuadVBO.render();
}

void TiltShiftFilter::pass2() {
    glViewport(0, 0, controlPanel->windowWidth, controlPanel->windowHeight);
    
    controlPanel->texRenderTarget.unuse();

    glActiveTexture(GL_TEXTURE0);
    controlPanel->texRenderTarget.getCurrentTexture().bind();
}

void TiltShiftFilter::resize(int width, int height) {

    texSize.x = width;
    texSize.y = height;

    if (oldWidth < 0.0f) {
        vec2 step = texSize / 4.0f;
        controlPointBegin = step;
        controlPointEnd = step * 3.0f;
        controlPointEnd.y = texSize.y / 2.0f;
    } else {

        float prcX = controlPointBegin.x / oldWidth;
        float prcY = controlPointBegin.y / oldHeight;

        controlPointBegin.x = prcX * texSize.x;
        controlPointBegin.y = prcY * texSize.y;

        prcX = controlPointEnd.x / oldWidth;
        prcY = controlPointEnd.y / oldHeight;
        controlPointEnd.x = prcX * texSize.x;
        controlPointEnd.y = prcY * texSize.y;
    }

    oldWidth = (float) width;
    oldHeight = (float) height;

    program->use();

    program->setUniform("texSize", texSize);
    program->setUniform("begin", controlPointBegin);
    program->setUniform("end", controlPointEnd);
}

void TiltShiftFilter::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setFilter(this);
}

void TiltShiftFilter::reset() {
    if (!program) {
        return;
    }

    blurRadius = DEFAULT_BLUR_RADIUS;
    drawControlPoints = true;

    program->use();
    program->setUniform("blurRadius", blurRadius);
    program->setUniform("gradientRadius", gradientRadius);
    program->setUniform("drawControlPoints", drawControlPoints);
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* TiltShiftFilter::getName() {
    return FILTER_GROUP;
}

void TW_CALL
TiltShiftFilter::setBlurRadiusCallback(const void* value, void* clientData) {
    TiltShiftFilter* filter = static_cast<TiltShiftFilter*> (clientData);
    filter->blurRadius = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("blurRadius", filter->blurRadius);
}

void TW_CALL
TiltShiftFilter::getBlurRadiusCallback(void* value, void* clientData) {
    TiltShiftFilter* filter = static_cast<TiltShiftFilter*> (clientData);

    *(float*) value = filter->blurRadius;
}

void TW_CALL
TiltShiftFilter::setGradientRadiusCallback(const void* value, void* clientData) {
    TiltShiftFilter* filter = static_cast<TiltShiftFilter*> (clientData);
    filter->gradientRadius = *(const float*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("gradientRadius", filter->gradientRadius);
}

void TW_CALL
TiltShiftFilter::getGradientRadiusCallback(void* value, void* clientData) {
    TiltShiftFilter* filter = static_cast<TiltShiftFilter*> (clientData);

    *(float*) value = filter->gradientRadius;
}

void TW_CALL
TiltShiftFilter::setDrawCenterCallback(const void* value, void* clientData) {
    TiltShiftFilter* filter = static_cast<TiltShiftFilter*> (clientData);
    filter->drawControlPoints = *(const bool*) value;

    if (!filter->program || !filter->amILoaded()) {
        return;
    }

    filter->program->setUniform("drawControlPoints", filter->drawControlPoints);
}

void TW_CALL
TiltShiftFilter::getDrawCenterCallback(void* value, void* clientData) {
    TiltShiftFilter* filter = static_cast<TiltShiftFilter*> (clientData);

    *(bool*) value = filter->drawControlPoints;
}