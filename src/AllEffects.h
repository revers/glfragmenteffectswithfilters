/* 
 * File:   AllEffects.h
 * Author: Revers
 *
 * Created on 29 maj 2012, 08:00
 */

#ifndef ALLEFFECTS_H
#define	ALLEFFECTS_H

#include "Effects/AbstractEffect.h"
//#include "Effects/ReliefTunnelEffect.h"
//#include "Effects/MonjoriEffect.h"
//#include "Effects/KaleidoscopeEffect.h"
//#include "Effects/DeformEffect.h"
//#include "Effects/SquareTunnelEffect.h"
//#include "Effects/FlyEffect.h"
//#include "Effects/PlasmaEffect.h"
//#include "Effects/PulseEffect.h"
//#include "Effects/WaterEffect.h"
//#include "Effects/ShapesEffect.h"
//#include "Effects/MetablobEffect.h"
//#include "Effects/SevenZeroFourEffect.h"
//#include "Effects/LandscapeEffect.h"
//#include "Effects/AppleEffect.h"
#include "Effects/TheRoadOfRibbonEffect.h"
#include "Effects/NautilusEffect.h"
//#include "Effects/MengerSpongeEffect.h"
#include "Effects/ClodEffect.h"
#include "Effects/SlisesixEffect.h"
#include "Effects/SultEffect.h"
//#include "Effects/ValleyballEffect.h"
#include "Effects/RedEffect.h"
#include "Effects/QuaternionEffect.h"
#include "Effects/MetaTunnel.h"
//#include "Effects/DroidEffect.h"
#include "Effects/LeizexEffect.h"
#include "Effects/KinderpainterEffect.h"
#include "Effects/MandelbulbEffect.h"
#include "Effects/DiscoEffect.h"

#endif	/* ALLEFFECTS_H */

