/* 
 * File:   TheRoadOfRibbonEffect.cpp
 * Author: Revers
 * 
 * Created on 19 czerwiec 2012, 19:57
 */

#include <iostream>
#include <GL/glew.h>
#include <GL/gl.h>

#include "TheRoadOfRibbonEffect.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define EFFECT_GROUP "The Road of Ribbon"
#define EFFECT_PREFIX "tror_"
#define VERTEX_SHADER_FILE "shaders/Default.vert"
#define FRAGMENT_SHADER_FILE "shaders/TheRoadOfRibbon.frag"

Logger TheRoadOfRibbonEffect::logger = Logger::getInstance("effects.TheRoadOfRibbon");

TheRoadOfRibbonEffect::TheRoadOfRibbonEffect(ControlPanel* controlPanel)
: AbstractEffect(controlPanel) {
    //antiAliasingOn = false;
}

bool TheRoadOfRibbonEffect::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderFromFile(VERTEX_SHADER_FILE,
            GLSLShader::VERTEX)) {
        LOG_ERROR(logger, "Compilation of file " VERTEX_SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->compileShaderFromFile(FRAGMENT_SHADER_FILE,
            GLSLShader::FRAGMENT)) {
        LOG_ERROR(logger, "Compilation of file " FRAGMENT_SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking program FAILED!!");
        return false;
    }

    addLoadButton(EFFECT_PREFIX "effect", EFFECT_GROUP);

    int opened = 0;
    TwSetParam(effectBar, EFFECT_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void TheRoadOfRibbonEffect::reset() {
    if (!program) {
        return;
    }

    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* TheRoadOfRibbonEffect::getName() {
    return EFFECT_GROUP;
}

void TheRoadOfRibbonEffect::use() {
    cout << "TheRoadOfRibbonEffect::use()" << endl;
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setEffect(this);
}

void TheRoadOfRibbonEffect::resize(int x, int y) {
    program->use();
    float aspectRatio = (float) x / (float) y;
    program->setUniform("AspectRatio", aspectRatio);
}