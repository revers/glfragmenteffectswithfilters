/* 
 * File:   QuaternionEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 21:49
 */

#ifndef QUATERNIONEFFECT_H
#define	QUATERNIONEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class QuaternionEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    QuaternionEffect(ControlPanel* controlPanel);

    virtual ~QuaternionEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* QUATERNIONEFFECT_H */

