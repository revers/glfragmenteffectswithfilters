/* 
 * File:   KinderpainterEffect.cpp
 * Author: Revers
 * 
 * Created on 20 czerwiec 2012, 07:24
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include "KinderpainterEffect.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define EFFECT_GROUP "Kinderpainter"
#define EFFECT_PREFIX "Kinderpainter_"
#define VERTEX_SHADER_FILE "shaders/Default.vert"
#define FRAGMENT_SHADER_FILE "shaders/Kinderpainter.frag"

Logger KinderpainterEffect::logger = Logger::getInstance("effects.Kinderpainter");

KinderpainterEffect::KinderpainterEffect(ControlPanel* controlPanel)
: AbstractEffect(controlPanel) {
}

bool KinderpainterEffect::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderFromFile(VERTEX_SHADER_FILE,
            GLSLShader::VERTEX)) {
        LOG_ERROR(logger, "Compilation of file " VERTEX_SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->compileShaderFromFile(FRAGMENT_SHADER_FILE,
            GLSLShader::FRAGMENT)) {
        LOG_ERROR(logger, "Compilation of file " FRAGMENT_SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking program FAILED!!");
        return false;
    }

    addLoadButton(EFFECT_PREFIX "effect", EFFECT_GROUP);

    int opened = 0;
    TwSetParam(effectBar, EFFECT_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void KinderpainterEffect::mouseAction(int x, int y) {
    float xNormalized = -1.0f + 2.0f * ((float) x / controlPanel->imageWidth);
    float yNormalized = -1.0f + 2.0f * ((float) y / controlPanel->imageHeight);

    program->use();
    program->setUniform("Mouse", vec2(xNormalized, yNormalized));
}

void KinderpainterEffect::reset() {
    if (!program) {
        return;
    }

    program->use();
    program->setUniform("Mouse", vec2(0));

    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* KinderpainterEffect::getName() {
    return EFFECT_GROUP;
}

void KinderpainterEffect::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setEffect(this);
}

void KinderpainterEffect::resize(int x, int y) {
    program->use();
    float aspectRatio = (float) x / (float) y;
    program->setUniform("AspectRatio", aspectRatio);

    program->setUniform("Resolution", vec2(x, y));
}

