/* 
 * File:   LeizexEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 22:20
 */

#ifndef LEIZEXEFFECT_H
#define	LEIZEXEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class LeizexEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    LeizexEffect(ControlPanel* controlPanel);

    virtual ~LeizexEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* LEIZEXEFFECT_H */

