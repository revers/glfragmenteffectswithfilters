/* 
 * File:   MetaTunnel.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 21:57
 */

#ifndef METATUNNEL_H
#define	METATUNNEL_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class MetaTunnelEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    MetaTunnelEffect(ControlPanel* controlPanel);

    virtual ~MetaTunnelEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* METATUNNEL_H */

