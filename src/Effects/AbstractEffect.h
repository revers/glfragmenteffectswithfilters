/* 
 * File:   AbstractEffect.h
 * Author: Revers
 *
 * Created on 16 czerwiec 2012, 21:50
 */

#ifndef ABSTRACT_EFFECT_H
#define	ABSTRACT_EFFECT_H

#include "../LoggingDefines.h"
#include <rev/gl/RevGLSLProgram.h>

#include <AntTweakBar.h>
#include <boost/shared_ptr.hpp>

class ControlPanel;

typedef boost::shared_ptr<rev::GLSLProgram> GLSLProgramPtr;

class AbstractEffect {
    friend class ControlPanel;
protected:
    ControlPanel* controlPanel;
    TwBar* effectBar;
    GLSLProgramPtr program;
    float time;
    bool antiAliasingOn;
public:
    AbstractEffect(ControlPanel* controlPanel_);

    virtual ~AbstractEffect() {
    }

    virtual bool init() = 0;
    virtual void reset() = 0;
    virtual const char* getName() = 0;
    virtual void use();
    virtual void apply();

    virtual void setTime(float time) {
        this->time = time;
        program->use();
        program->setUniform("Time", time);
    }

    virtual void resize(int width, int height) {
    }

    virtual void mousePressed(int x, int y) {
    }

    virtual void mouseReleased(int x, int y) {
    }

    virtual void mouseDragged(int x, int y) {
    }

    virtual void mouseWheelCallback(int diff) {
    }

    bool isAntiAliasingOn() const {
        return antiAliasingOn;
    }

    void setAntiAliasingOn(bool antiAliasingOn) {
        this->antiAliasingOn = antiAliasingOn;
    }

protected:
    void addLoadButton(const char* twName, const char* twGroup);

    bool amILoaded();
private:
    static void TW_CALL loadButtonCallback(void* clientData);
};


#endif	/* ABSTRACT_FILTER_H */

