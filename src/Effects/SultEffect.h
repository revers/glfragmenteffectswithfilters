/* 
 * File:   SultEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 20:51
 */

#ifndef SULTEFFECT_H
#define	SULTEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class SultEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    SultEffect(ControlPanel* controlPanel);

    virtual ~SultEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* SULTEFFECT_H */

