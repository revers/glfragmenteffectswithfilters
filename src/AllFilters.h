/* 
 * File:   AllFilters.h
 * Author: Revers
 *
 * Created on 29 maj 2012, 08:00
 */

#ifndef ALLFILTERS_H
#define	ALLFILTERS_H

#include "Filters/AbstractFilter.h"
#include "Filters/NoFilter.h"
#include "Filters/GaussianBlurFilter.h"
#include "Filters/TriangleBlurFilter.h"
#include "Filters/ZoomBlurFilter.h"
#include "Filters/TiltShiftFilter.h"
#include "Filters/HexagonalPixelateFilter.h"
#include "Filters/LensBlurFilter.h"
#include "Filters/CameraFilter.h"
#include "Filters/BloomFilter.h"

#endif	/* ALLFILTERS_H */

