#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 400

layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}
/**
 * Fragment shader from "OpenGL 4.0 Shading Language Cookbook" by David Wolff.
 */
//==============================================================================
//-- Fragment
#version 400

in vec2 TexCoord;

subroutine vec4 RenderPassType();
subroutine uniform RenderPassType RenderPass;

uniform sampler2D Tex1;
uniform sampler2D Tex2;

uniform int Width;
uniform int Height;
uniform float PixOffset[5] = float[](0.0, 1.0, 2.0, 3.0, 4.0);
uniform float Weight[5]; // = float[](0.2270270270, 0.1945945946, 0.1216216216,
//0.0540540541, 0.0162162162);

uniform float LumThresh; // Luminance threshold

layout(location = 0) out vec4 FragColor;


float luminance(vec3 color) {
    return 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Trick for full support in NetBeans IDE.
// I'm pretending that this is C++ file.
#ifdef FAKE_INCLUDE
#    undef subroutine
#    define subroutine(X)
#endif
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Pass to extract the bright parts

subroutine(RenderPassType)
vec4 pass1() {
    vec4 val = texture(Tex1, TexCoord);
    if (luminance(val.rgb) > LumThresh)
        return val * 0.5;
    else
        return vec4(0.0);
}

// gaussian blur pass #1

subroutine(RenderPassType)
vec4 pass2() {
    float dy = 1.0 / float(Height);

    vec4 sum = texture(Tex2, TexCoord) * Weight[0];
    for (int i = 1; i < 10; i++) {
        sum += texture(Tex2, TexCoord + vec2(0.0, PixOffset[i]) * dy) * Weight[i];
        sum += texture(Tex2, TexCoord - vec2(0.0, PixOffset[i]) * dy) * Weight[i];
    }
    return sum;
}

// gaussian blur pass #2

subroutine(RenderPassType)
vec4 pass3() {
    float dx = 1.0 / float(Width);

    vec4 val = texture(Tex1, TexCoord);
    vec4 sum = texture(Tex2, TexCoord) * Weight[0];
    for (int i = 1; i < 10; i++) {
        sum += texture(Tex2, TexCoord + vec2(PixOffset[i], 0.0) * dx) * Weight[i];
        sum += texture(Tex2, TexCoord - vec2(PixOffset[i], 0.0) * dx) * Weight[i];
    }
    return val + (sum * sum.a);
}

void main() {
    FragColor = RenderPass();
}
