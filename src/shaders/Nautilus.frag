#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;
uniform float Time = 0.0f;

//uniform float AspectRatio = 1.0f;

uniform float AAScale;
uniform int AALevel = 1;
uniform float InvAALevel;

out vec4 FragColor;

float e(vec3 c) {
    c = cos(vec3(cos(c.r + Time / 6.0) * c.r - cos(c.g * 3.0 + Time / 5.0) * c.g, cos(Time / 4.0) * c.b / 3.0 * c.r - cos(Time / 7.0) * c.g, c.r + c.g + c.b + Time));
    return dot(c*c, vec3(1.0)) - 1.0;
}

vec3 getPixelColor(vec2 c) {

    vec3 o = vec3(c.r, c.g, 0.0), g = vec3(c.r, c.g, 1.0) / 64.0, v = vec3(0.5);
    float m = 0.4;
    //    float m = 1.0-1.5*mouse.x/resolution.x;

    for (int r = 0; r < 100; r++) {
        float h = e(o) - m;
        if (h < 0.0)break;
        o += h * 10.0 * g;
        v += h * 0.02;
    }
    // light (who needs a normal?)
    v += e(o + 0.1) * vec3(0.4, 0.7, 1.0);

    // ambient occlusion
    float a = 0.0;
    for (int q = 0; q < 100; q++) {
        float l = e(o + 0.5 * vec3(cos(1.1 * float(q)), cos(1.6 * float(q)), cos(1.4 * float(q)))) - m;
        a += clamp(4.0 * l, 0.0, 1.0);
    }
    v *= a / 100.0;
    return v;
}

void main() {

    vec2 p = -1.0 + 2.0 * TexCoord;

    if (AALevel == 1) {
        FragColor = vec4(getPixelColor(p), 1.0f);
    } else {
        vec3 color = vec3(0.0, 0.0, 0.0);

        for (int x = 0; x < AALevel; x++) {
            for (int y = 0; y < AALevel; y++) {
                color += getPixelColor(p + vec2(x, y) * InvAALevel * AAScale);
            }
        }

        FragColor = vec4(color * InvAALevel * InvAALevel, 1.0f);
    }
}
