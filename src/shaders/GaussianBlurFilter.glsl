#include <FAKE_INCLUDE/glsl_fake.hxx>

//$END_FAKE$

//-- Vertex
#version 400

layout(location = 0) in vec3 VertexPosition;
layout(location = 1) in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition, 1.0);
}
/**
 * Fragment shader from "OpenGL 4.0 Shading Language Cookbook" by David Wolff.
 */
//==============================================================================
//-- Fragment
#version 400

in vec2 TexCoord;

subroutine vec4 RenderPassType();
subroutine uniform RenderPassType RenderPass;

uniform sampler2D Tex1;

uniform int Width;
uniform int Height;
uniform float PixOffset[5] = float[](0.0, 1.0, 2.0, 3.0, 4.0);
uniform float Weight[5]; // = float[](0.2270270270, 0.1945945946, 0.1216216216,
//0.0540540541, 0.0162162162);

layout(location = 0) out vec4 FragColor;

// Trick for full support in NetBeans IDE.
// I'm pretending that this is C++ file.
#ifdef FAKE_INCLUDE
#    undef subroutine
#    define subroutine(X)
#endif

subroutine(RenderPassType)
vec4 pass1() {
    float dy = 1.0 / float(Height);

    vec4 sum = texture(Tex1, TexCoord) * Weight[0];
    for (int i = 1; i < 5; i++) {
        sum += texture(Tex1, TexCoord + vec2(0.0, PixOffset[i]) * dy) * Weight[i];
        sum += texture(Tex1, TexCoord - vec2(0.0, PixOffset[i]) * dy) * Weight[i];
    }
    return sum;
}

subroutine(RenderPassType)
vec4 pass2() {
    float dx = 1.0 / float(Width);

    vec4 sum = texture(Tex1, TexCoord) * Weight[0];
    for (int i = 1; i < 5; i++) {
        sum += texture(Tex1, TexCoord + vec2(PixOffset[i], 0.0) * dx) * Weight[i];
        sum += texture(Tex1, TexCoord - vec2(PixOffset[i], 0.0) * dx) * Weight[i];
    }
    return sum;
}

void main() {
    FragColor = RenderPass();
}
